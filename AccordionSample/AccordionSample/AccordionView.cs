﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xamarin.Forms;

namespace AccordionSample
{
    public class InnerTemplate : AbsoluteLayout
    {
        public string _textColor = "#333333";
        public string _separatorLineColor = "#CCCCCC";
        public InnerTemplate()
        {
            Grid grid = new Grid();
            grid.RowDefinitions = new RowDefinitionCollection()
            {
                new RowDefinition {Height = 35 }
            };

            grid.ColumnDefinitions = new ColumnDefinitionCollection();
            this.HeightRequest = 35;

            var link = new Label
            {
                FontSize = 13,
                FontFamily = "Roboto",
                Margin = new Thickness(15, 0, 0, 0),
                TextColor = Color.FromHex(_textColor),
                HorizontalTextAlignment = TextAlignment.Start,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            var box = new BoxView
            {
                Color = Color.FromHex(_separatorLineColor),
                HeightRequest = 1,
                VerticalOptions = LayoutOptions.EndAndExpand
            };

            grid.Children.Add(link, 0, 0);
            grid.Children.Add(box, 0, 0);

            this.Children.Add(grid, new Rectangle(0, 0, 1, 1), AbsoluteLayoutFlags.All);

            link.SetBinding(Label.TextProperty, "Link");
        }
    }

    public class AccordionView : ScrollView
    {
        public string _iconHome = "home.png";
        public string _textColor = "#333333";
        private StackLayout _layout = new StackLayout { Spacing = 0 };

        public DataTemplate Template { get; set; }
        public DataTemplate SubTemplate { get; set; }

        public static Label nameLabel;

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(
                propertyName: "ItemsSource",
                returnType: typeof(IList),
                declaringType: typeof(AccordionSectionView),
                defaultValue: default(IList),
                propertyChanged: AccordionView.PopulateList);

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public AccordionView(DataTemplate itemTemplate, Delegate del)
        {
            this.SubTemplate = itemTemplate;
            this.Template = new DataTemplate(() => (object)(new AccordionSectionView(itemTemplate, this, del)));
            this.Content = _layout;
        }

        void PopulateList()
        {
            _layout.Children.Clear();

            var homeStackPanel = new StackLayout() { BackgroundColor = Color.FromHex("#CCCCCC"), VerticalOptions = LayoutOptions.Start, Orientation = StackOrientation.Horizontal, HeightRequest = 40 };
            homeStackPanel.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        MainPage.menuPage.Detail = new NavigationPage(new LandingPage());
                        MainPage.menuPage.IsPresented = false;
                        AccordionSectionView.UpdateActiveMenuItem("");

                    })
                }
            );


            nameLabel = new Label() { Margin = new Thickness(5, 0, 0, 0), FontSize = 15, FontFamily = "Roboto", TextColor = Color.FromHex(_textColor), VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.StartAndExpand };
            var homeImage = new Image() { Source = _iconHome, Margin = new Thickness(0, 0, 10, 0), HeightRequest = 30, WidthRequest = 30, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalOptions = LayoutOptions.EndAndExpand };

            if (App.Current.Properties.ContainsKey("FirstName") && App.Current.Properties.ContainsKey("LastName"))
            {
                nameLabel.Text = App.Current.Properties["FirstName"].ToString() + App.Current.Properties["LastName"].ToString();
            }
            homeStackPanel.Children.Add(nameLabel);
            homeStackPanel.Children.Add(homeImage);

            _layout.Children.Add(homeStackPanel);

            foreach (object item in this.ItemsSource)
            {
                var template = (View)this.Template.CreateContent();
                template.BindingContext = item;
                _layout.Children.Add(template);
            }
        }

        static void PopulateList(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionView)bindable).PopulateList();
        }
    }

    public class AccordionSectionView : StackLayout
    {
        private string __headerBGColor = "#2293FF";
        private bool _isExpanded = false;
        private StackLayout _content = new StackLayout
        {
            HeightRequest = 0,
            Spacing = 0,
            Margin = new Thickness(0),
            Padding = new Thickness(0, 0, 0, 0),
            VerticalOptions = LayoutOptions.CenterAndExpand
        };
        private Color _headerColor;
        private ImageSource _arrowRight = "arrow_right_white.png";
        private ImageSource _arrowDown = "arrow_down_white.png";
        private AbsoluteLayout _header = new AbsoluteLayout();
        private Image _headerIcon = new Image { VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.EndAndExpand };
        private Label _headerTitle = new Label { FontFamily = "Roboto", FontSize = 15, TextColor = Color.FromHex("#FFF"), VerticalTextAlignment = TextAlignment.Center, HeightRequest = 30 };
        public DataTemplate _template;
        public static View _prevMenuItem = null;
        public static Dictionary<string, View> _menuLinkTemplates = new Dictionary<string, View>();

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(
                propertyName: "ItemsSource",
                returnType: typeof(IList),
                declaringType: typeof(AccordionSectionView),
                defaultValue: default(IList),
                propertyChanged: AccordionSectionView.PopulateList);

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly BindableProperty TitleProperty =
            BindableProperty.Create(
                propertyName: "Title",
                returnType: typeof(string),
                declaringType: typeof(AccordionSectionView),
                propertyChanged: AccordionSectionView.ChangeTitle);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public Delegate _menuLinkClickDel;

        public AccordionSectionView(DataTemplate itemTemplate, ScrollView parent, Delegate del)
        {
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    _headerColor = Color.FromHex(__headerBGColor);
                    break;
                case Device.Android:
                    _headerColor = Color.FromHex(__headerBGColor);
                    break;
            }

            _menuLinkClickDel = del;

            _template = itemTemplate;

            _headerTitle.BackgroundColor = _headerColor;
            _headerIcon.Source = "arrow_down_white.png";
            _headerIcon.HeightRequest = 15;
            _header.BackgroundColor = _headerColor;
            _headerIcon.Margin = new Thickness(0, 0, 10, 0);
            _headerTitle.Margin = new Thickness(5, 0, 0, 0);
            _header.Children.Add(_headerIcon, new Rectangle(1, 0.8, 0.1, 1), AbsoluteLayoutFlags.All);
            _header.Children.Add(_headerTitle, new Rectangle(0.1, 1, 0.9, 1), AbsoluteLayoutFlags.All);

            this.Spacing = 0;
            this.Children.Add(_header);
            this.Children.Add(_content);

            _header.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        if (_isExpanded)
                        {
                            _headerIcon.Source = "arrow_right_white.png";
                            _content.HeightRequest = 0;
                            _content.IsVisible = false;
                            _isExpanded = false;
                        }
                        else
                        {
                            _headerIcon.Source = "arrow_down_white.png";
                            _content.HeightRequest = _content.Children.Count * 35;
                            _content.IsVisible = true;
                            _isExpanded = true;
                        }
                    })
                }
            );
        }

        void ChangeTitle()
        {
            _headerTitle.Text = this.Title;
        }

        /// <summary>
        /// method used to Highlight menu item in the NavigationMenu
        /// method calls from landing page will also effect the NavigationMenu 
        /// </summary>
        /// <param name="activeMenuItem"></param>
        public static void UpdateActiveMenuItem(string activeMenuItem)
        {
            var currentMenuItem = _menuLinkTemplates.ContainsKey(activeMenuItem) ? _menuLinkTemplates[activeMenuItem] : null;

            if (_prevMenuItem != null)
            {
                _prevMenuItem.BackgroundColor = Color.Transparent;
            }

            if (currentMenuItem != null)
            {
                currentMenuItem.BackgroundColor = Color.FromHex("#CCCCCC");
                _prevMenuItem = currentMenuItem;
            }
        }

        void PopulateList()
        {
            _content.Children.Clear();

            foreach (var item in ItemsSource)
            {
                var template = (View)_template.CreateContent();
                template.BindingContext = item;

                string link = ((MenuLink)item).Link;
                if (!_menuLinkTemplates.ContainsKey(link))
                {
                    _menuLinkTemplates.Add(link, template);
                }
                else
                {
                    _menuLinkTemplates[link] = template;
                }

                _content.Children.Add(template);

                template.GestureRecognizers.Add(
                new TapGestureRecognizer
                {
                    Command = new Command(() =>
                    {
                        MenuLink menuItem = (MenuLink)item;
                        UpdateActiveMenuItem(menuItem.Link);
                        _menuLinkClickDel.DynamicInvoke(menuItem);
                    })
                }
            );
            }
            _content.HeightRequest = _content.Children.Count * 35;
            _content.IsVisible = true;
            _isExpanded = true;
        }

        static void ChangeTitle(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionSectionView)bindable).ChangeTitle();
        }

        static void PopulateList(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionSectionView)bindable).PopulateList();
        }
    }
}