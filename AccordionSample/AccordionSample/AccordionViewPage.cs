﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AccordionSample
{
    public class MenuLink
    {
        public string Link { get; set; }

        public string ViewName { get; set; }
    }

    public class MenuSection 
    {
        public string Title { get; set; }
        public IEnumerable<MenuLink> List { get; set; }
    }

    public class ViewModel
    {
        public IEnumerable<MenuSection> List { get; set; }
    }

    public class AccordionViewPage : ContentPage
    {
        public void GetMenu()
        {
            try
            {
                MenuLinkClick del = new MenuLinkClick(GetClickedItem);

                var template = new DataTemplate(typeof(InnerTemplate));

                var view = new AccordionView(template, del);
                view.SetBinding(AccordionView.ItemsSourceProperty, "List");
                view.Template.SetBinding(AccordionSectionView.TitleProperty, "Title");
                view.Template.SetBinding(AccordionSectionView.ItemsSourceProperty, "List");

                List<MenuSection> jsonData = new List<MenuSection>();

                jsonData.Add(new MenuSection
                {
                    Title = "Animals",
                    List = new List<MenuLink>()
                    {
                        new MenuLink()
                        {
                            Link= "CAT",
                            ViewName="Cat page"
                        }, 
                        new MenuLink()
                        {
                            Link="DOG",
                            ViewName="Dog page"
                        },
                        new MenuLink()
                        {
                            Link= "WOLF",
                            ViewName="Wolf page"
                        },
                        new MenuLink()
                        {
                            Link="ZEBRA",
                            ViewName="Zebra page"
                        },new MenuLink()
                        {
                            Link= "ELEPHANT",
                            ViewName="Elephant Page"
                        },
                        new MenuLink()
                        {
                            Link="ANT",
                            ViewName="Ant page"
                        }
                    }
                });

                jsonData.Add(new MenuSection
                {
                    Title = "Vehicles",
                    List = new List<MenuLink>()
                    {
                        new MenuLink()
                        {
                            Link= "CAR",
                            ViewName="Car page"
                        },
                        new MenuLink()
                        {
                            Link="Truck",
                            ViewName="Truck Page"
                        },
                        new MenuLink()
                        {
                            Link= "Jeep",
                            ViewName="Jeep page"
                        },
                        new MenuLink()
                        {
                            Link="Bike",
                            ViewName="Bike page"
                        },new MenuLink()
                        {
                            Link= "Activa",
                            ViewName="Activa page"
                        }
                    }
                });

                jsonData.Add(new MenuSection
                {
                    Title = "Company",
                    List = new List<MenuLink>()
                    {
                        new MenuLink()
                        {
                            Link= "GGK",
                            ViewName="GGk page"
                        },
                        new MenuLink()
                        {
                            Link="Accenture",
                            ViewName="Accenture page"
                        },
                        new MenuLink()
                        {
                            Link= "CapGemini",
                            ViewName="capGemini page"
                        },
                        new MenuLink()
                        {
                            Link="TCS",
                            ViewName="TCS page"
                        },new MenuLink()
                        {
                            Link= "ADP",
                            ViewName="ADP page"
                        },
                        new MenuLink()
                        {
                            Link="BirlaSoft",
                            ViewName="BirlaSoft page"
                        }
                    }
                });

                //jsonData.Add(new MenuSection
                //{
                //    Title = "Animals",
                //    List = new List<MenuLink>()
                //    {
                //        new MenuLink()
                //        {
                //            Link= "CAT",
                //            ViewName=""
                //        },
                //        new MenuLink()
                //        {
                //            Link="DOG",
                //            ViewName=""
                //        },
                //        new MenuLink()
                //        {
                //            Link= "WOLF",
                //            ViewName=""
                //        },
                //        new MenuLink()
                //        {
                //            Link="ZEBRA",
                //            ViewName=""
                //        },new MenuLink()
                //        {
                //            Link= "ELEPHANT",
                //            ViewName=""
                //        },
                //        new MenuLink()
                //        {
                //            Link="ANT",
                //            ViewName=""
                //        }
                //    }
                //});

                view.BindingContext = new ViewModel { List = jsonData };
                this.Content = view;
            }
            catch (Exception ex)
            {
               
            }
        }

        public delegate void MenuLinkClick(object item);

        public event EventHandler MenuLinkClickEvent;

        public void GetClickedItem(object item)
        {
            MenuLinkClickEvent(item, null);
        }

        public AccordionViewPage()
        {
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    Padding = new Thickness(0, 20, 0, 0);
                    break;
                case Device.Android:
                    Padding = new Thickness(0, 22, 0, 0);
                    break;
                default:
                    break;
            }

            this.Title = "AccordionSample";
            //this.Icon = StringHelper._iconHambuger;

            GetMenu();
        }
    }
}