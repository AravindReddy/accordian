﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AccordionSample
{
    public partial class MainPage : MasterDetailPage
    {
        public static MainPage menuPage;
        public MainPage()
        {
            InitializeComponent();

            menuPage = this;

            try
            {
                AccordionViewPage view = new AccordionViewPage();

                LandingPage landing = new LandingPage();

                this.Detail = new NavigationPage(landing);
                NavigationPage.SetHasNavigationBar(this, false);
                view.MenuLinkClickEvent += View_MenuLinkClickEvent;

                this.Master = view;
                this.Master.BackgroundColor = Color.FromHex("#EEEEEE");

                this.IsPresentedChanged += (sender, e) =>
                {
                    if (this.IsPresented == true)
                    {
                        this.Detail.InputTransparent = true;
                    }
                    else
                    {
                        this.Detail.InputTransparent = false;
                    }
                };
            }
            catch (Exception ex)
            {

            }
        }

        
        private void View_MenuLinkClickEvent(object sender, EventArgs e)
        {
            //try
            //{
            //    MenuLink menuLink = sender as MenuLink;
            //    Type selectedPage = Type.GetType(menuLink.ViewName);
            //    Page currentPage = (Page)Activator.CreateInstance(selectedPage);

            //    var page = new NavigationPage(currentPage);
            //    NavigationPage.SetHasNavigationBar(this, false);

            //    this.Detail = page;
            //    this.IsPresented = false;
            //}
            //catch (Exception ex)
            //{

            //}
        }
    }
}
